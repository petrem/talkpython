{ pkgs ?
  (let sources = import nix/sources.nix;
   in import sources.nixpkgs
     {config.allowUnfree = true;})}:

with pkgs;
let
  inherit (lib) optional optionals;
  python = pkgs.python311;
  pythonPackages = pkgs.python311Packages;
in

mkShell {
  buildInputs = [
    (
      import ./nix/default.nix { inherit pkgs;
                                 inherit python;
                                 inherit pythonPackages;
                                 extra_python_packages = with pythonPackages; [
                                   argcomplete
                                   pip      # to explore around...
                                   ipython  # a repl is always good
#                                   hatch
                                 ];
                               }
    )
    direnv
    entr
    git
    hatch
    just
  ] ++ optional stdenv.isLinux inotify-tools
  ++ optional stdenv.isDarwin terminal-notifier
  ++ optionals stdenv.isDarwin (with darwin.apple_sdk.frameworks; [
    CoreFoundation
    CoreServices
  ]);
}
