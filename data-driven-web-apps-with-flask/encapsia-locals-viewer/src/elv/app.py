import flask



# -- "model" follows

from itertools import groupby
from operator import itemgetter


def _groupby(iterable, key):
    yield from groupby(sorted(iterable, key=key), key=key)


def _get_plugins():
    import 
    _plugins = [
        {
            "name": name,
            "versions": [p["version"] for p in ps],
        }
        for name, ps in _groupby(PLUGINS, itemgetter("name"))
    ]
    return _plugins


PLUGINS = [
    {
        "name": "entry",
        "version": "1.10.0",
        "location": "file:///home/pmierlutiu/.encapsia/plugins/plugin-entry-1.10.0.tar.gz",
    },
    {
        "name": "entry",
        "version": "2.0.4",
        "location": "file:///home/pmierlutiu/.encapsia/plugins/plugin-entry-2.0.4.tar.gz",
    },
    {
        "name": "conduct",
        "version": "1.6.0",
        "location": "file:///home/pmierlutiu/.encapsia/plugins/plugin-conduct-1.6.0.tar.gz",
    },
]

# --

app = flask.Flask("elv")


@app.route("/")
def index():
    return flask.render_template("home/index.html")


@app.route("/plugins")
def plugins():
    return flask.render_template("plugins/plugins.html", plugins=_get_plugins())


if __name__ == "__main__":
    app.run(debug=True)
