# Encapsia Locals Viewer

[![PyPI - Version](https://img.shields.io/pypi/v/encapsia-locals-viewer.svg)](https://pypi.org/project/encapsia-locals-viewer)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/encapsia-locals-viewer.svg)](https://pypi.org/project/encapsia-locals-viewer)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install encapsia-locals-viewer
```

## License

`encapsia-locals-viewer` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
