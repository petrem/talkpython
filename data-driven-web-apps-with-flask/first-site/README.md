# first site

[![PyPI - Version](https://img.shields.io/pypi/v/first-site.svg)](https://pypi.org/project/first-site)
[![PyPI - Python Version](https://img.shields.io/pypi/pyversions/first-site.svg)](https://pypi.org/project/first-site)

-----

**Table of Contents**

- [Installation](#installation)
- [License](#license)

## Installation

```console
pip install first-site
```

## License

`first-site` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
