# talkpython

Scratchpad for the Talk Python courses.

## Test and Deploy

Maybe I want to play with some of this stuff...

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

* nix, python, flask, whatever -- always nice to have them somewhere to copy from?

## License

Nobody should use this stuff, it's just a scratchpad. 

Some of the content is cloned from some courses -- that code is owned and
licensed as the original author(s) intended, check case by case.

My own code from this repository is to be considered public domain unless otherwise
indicated.

## Dev environment

1. install nix (link, instructions)
2. install direnv (link, instructions)

Trying to use https://github.com/nmattia/niv to manage the channel but keep my older nix
setup (I'm not familiar enough with Nix, go easy).

So the niv-generated and maintaind files are imported into default.nix.

### Building hatch 

(This doesn't yet work, don't try it at home).

`hatch` is not shipped by nix in `python311Packages` (it is as a stand-alone derivation, but that uses python310).
So I wrote `nix/hatch.nix`.

To test-build it:

    nix-build -E 'with import <nixpkgs> {}; pkgs.python311Packages.callPackage nix/hatch.nix {}'
    nix-build -E 'with import <nixpkgs> {}; pkgs.python311Packages.callPackage nix/hatch.nix {}'

