{ lib
, buildPythonApplication
, fetchPypi
# python dependencies
, click
, hatchling
, httpx
, hyperlink
, keyring
, packaging
, pexpect
, platformdirs
, pyperclip
, rich
, shellingham
, tomli
, tomlkit
, userpath
, virtualenv
}:

buildPythonApplication rec {
  pname = "hatch";
  version = "1.6.3";
  # 1.7.0 needs latest hatchling which isn't available in nixos 23.05

  src = fetchPypi {
    inherit pname version;
    sha256 = "650e671ba300318e498ef93bbe3b99b32ce14920764fb8753f89993f63eed79a";
    # 1.7.0 sha256 = "7afc701fd5b33684a6650e1ecab8957e19685f824240ba7458dcacd66f90fb46";
  };

  format = "pyproject";
  nativeBuildInputs = [ hatchling ];

  propagatedBuildInputs = [ click hatchling httpx hyperlink keyring packaging pyperclip pexpect platformdirs rich shellingham tomli tomlkit userpath virtualenv ];

  meta = with lib; {
    description = "Hatch is a modern, extensible Python project manager.";
    homepage = "https://hatch.pypa.io/latest/";
    license = licenses.mit;
    maintainers = with maintainers; [ breakds ];
  };
}
